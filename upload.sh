#!/bin/bash

set -e

if [[ $CI_COMMIT_REF_NAME == "master" ]]; then
    echo "Uploading..."
    anaconda -t ${CONDA_UPLOAD_TOKEN} upload --user lfortran $HOME/conda_root/conda-bld/linux-64/antlr4-python3-runtime-*.tar.bz2
else
    echo "Not in master, not uploading"
fi
